"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var kafkajs_1 = require("kafkajs");
function test() {
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        var kafka, producer, eachMessage;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log('Starting test...');
                    kafka = new kafkajs_1.Kafka({
                        brokers: ['localhost:9092']
                    });
                    console.log('Kafka created...');
                    producer = kafka.producer();
                    console.log('Kafka Producer created...');
                    return [4 /*yield*/, producer.connect()];
                case 1:
                    _a.sent();
                    console.log('Kafka Producer connected...');
                    producer.send({
                        topic: 'notifications',
                        messages: [{
                                key: 'my-key',
                                value: JSON.stringify({
                                    "channel": "MAIL",
                                    "options": {
                                        "to": "juli.cba@gmail.com",
                                        "from": "noreply@nestjs.com",
                                        "subject": "Testing Nest MailerModule",
                                        "template": "payment.approved.html"
                                    },
                                    "data": {
                                        "payment_order": {
                                            "payer_email": "agropago.test02@gmail.com",
                                            "amount": 100,
                                            "description": "Venta de Quimicos",
                                            "invoice_number": "234-234213",
                                            "items": [{
                                                    "item_id": 21212,
                                                    "description": "Kripton",
                                                    "amount": 5
                                                }]
                                        },
                                        "amount": 100,
                                        "payment_methods": [{
                                                "id": 2,
                                                "options": {
                                                    "token": "9b5c42ea-9680-4f11-acd9-caa88ccfcb7e",
                                                    "amount": 100,
                                                    "term_id": 1,
                                                    "installment_id": 0,
                                                    "token_agro": "123123",
                                                    "token_type": "T",
                                                    "card_info": [{
                                                            "name": "User Apidoc",
                                                            "bin": 448459,
                                                            "expiration_year": 25,
                                                            "expiration_month": 1,
                                                            "last_four_digits": 3090
                                                        }]
                                                }
                                            }]
                                    }
                                })
                            }]
                    });
                    console.log('Kafka Producer send message to notifications topic...');
                    eachMessage = function (_a) {
                        var /*topic, partition,*/ message = _a.message;
                        return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_b) {
                                console.log("Produer Each Message: " + message);
                                return [2 /*return*/];
                            });
                        });
                    };
                    return [2 /*return*/];
            }
        });
    });
}
test();
