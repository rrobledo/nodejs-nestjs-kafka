import { Producer, Kafka, KafkaConfig } from 'kafkajs';

async function test() {

    console.log('Starting test...');
    const kafka = new Kafka({
        brokers: ['localhost:9092']
    });
    console.log('Kafka created...');
    let producer = kafka.producer();
    console.log('Kafka Producer created...');
    await producer.connect()
    console.log('Kafka Producer connected...');
    

    producer.send({
        topic: 'notifications',
        messages: [{
            key: 'my-key',
            value: JSON.stringify({
                "channel": "MAIL",
                "options": {
                    "to": "juli.cba@gmail.com",
                    "from": "noreply@nestjs.com",
                    "subject": "Testing Nest MailerModule",
                    "template": "payment.approved.html"
                },
                "data": {
                    "payment_order": {
                        "payer_email": "agropago.test02@gmail.com",
                        "amount": 100,
                        "description": "Venta de Quimicos",
                        "invoice_number": "234-234213",
                        "items": [{
                            "item_id": 21212,
                            "description": "Kripton",
                            "amount": 5
                        }]
                    },
                    "amount": 100,
                    "payment_methods": [{
                        "id": 2,
                        "options": {
                            "token": "9b5c42ea-9680-4f11-acd9-caa88ccfcb7e",
                            "amount": 100,
                            "term_id": 1,
                            "installment_id": 0,
                            "token_agro": "123123",
                            "token_type": "T",
                            "card_info": [{
                                "name": "User Apidoc",
                                "bin": 448459,
                                "expiration_year": 25,
                                "expiration_month": 1,
                                "last_four_digits": 3090
                            }]
                        }
                    }]
                }
            
            })
        }]
    })
    console.log('Kafka Producer send message to notifications topic...');
    
    const eachMessage = async ({ /*topic, partition,*/ message }) => {
        console.log(`Produer Each Message: ${message}`); 
    }
    
}

test();
