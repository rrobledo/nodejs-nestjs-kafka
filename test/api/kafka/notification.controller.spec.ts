import { Test, TestingModule } from '@nestjs/testing';
import { Producer, Kafka, KafkaConfig } from 'kafkajs';
describe('AppController', () => {

    let producer = null;
    beforeAll(async () => {
        const kafka = new Kafka({
            brokers: ['localhost:9092']
        });
        producer = kafka.producer();

        await producer.connect()
    });

    afterAll(async () => {
        await producer.disconnect()
    })
    describe('root', () => {
        it('should send a message to notification service through kafka', done => {
            producer.send({
                topic: 'send-message',
                messages: [{
                    key: 'my-key',
                    value: JSON.stringify({ some: 'data' })
                }]
            })
          
            const eachMessage = async ({ /*topic, partition,*/ message }) => {
                console.log({
                key: message.key.toString(),
                value: JSON.parse(message.value.toString())
                }); 
                done();
          }
    });
  });
});
