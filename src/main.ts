import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from 'utils/http-exception.filter';
import { ErrorFilter } from 'utils/error.filter';
import { ValidationPipe } from '@nestjs/common';
import { LoggerService } from 'nest-logger';
import { Transport } from '@nestjs/common/enums/transport.enum';

async function restBootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new ErrorFilter());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.useLogger(app.get(LoggerService));
  app.enableCors();
  return app.listen(3000);
}

async function kafkaBootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.KAFKA,
    options: {
      client: {
        brokers: ['localhost:9092'],
      }
    }
  });
  return app.listen(() => console.log('Microservice is listening'));
}

async function bootstrap() {
  kafkaBootstrap()
  await restBootstrap()
}
bootstrap();
