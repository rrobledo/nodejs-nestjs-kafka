import { ControllersModule } from 'api/rest/controllers/controllers.module';
import { Module } from '@nestjs/common';
import configuration from 'config/configuration';

import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from 'utils/logger.module';
import { HandlebarsAdapter, MailerModule } from '@nestjs-modules/mailer';
import { KafkaModule } from 'api/kafka/kafka.module';


@Module({
  imports: 
    [LoggerModule,
    ControllersModule,
    KafkaModule,
    ConfigModule.forRoot({
                          load: [configuration],
                        }),
    MailerModule.forRootAsync({        
      useFactory: () => ({  
                transport: {
                  host: 'smtp.webfaction.com',
                  port: 1025,
                  ignoreTLS: true,
                  secure: false,
                  auth: {
                    user: 'webagropago',
                    pass: '$32Formac$!sub',
                  }
                },
                defaults: {
                  from: '"No Reply" <no-reply@agropago.com>',
                },
                preview: false,
                template: {
                  adapter: new HandlebarsAdapter(),
                  options: {
                    strict: true,
                  },
                },
              })
            })
          ]
})
export class AppModule {}
