import { Module } from '@nestjs/common';
import { LoggerModule } from 'utils/logger.module';
import { ServicesModule } from 'services/services.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { NotificationController } from 'api/kafka/notification.controller';
import { NotificationService } from 'services/notification.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    LoggerModule, 
    ConfigModule,
    ServicesModule,
    ClientsModule.register([
      {
        name: 'NOTIFICATION_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'notification-controller',
            brokers: ['localhost:9092'],
          },
          consumer: {
            groupId: 'notifications-consumer-group'
          }
        }
      },
    ]),
  ],
  controllers: [NotificationController],
  providers: [NotificationService],
  exports: [],
})
export class KafkaModule {}
