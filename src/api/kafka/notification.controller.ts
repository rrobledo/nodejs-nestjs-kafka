
import { Controller, Inject } from '@nestjs/common';
import { Kafka, Consumer } from 'kafkajs';
import { Payload, MessagePattern, ClientKafka } from '@nestjs/microservices';
import { NotificationService } from 'services/notification.service';
import { Subscriber } from 'rxjs';
import {ConsumerConfig} from "kafkajs";
@Controller()
export class NotificationController {

    consumer: Consumer = null;
    constructor( private readonly notificationService: NotificationService, 
        /*@Inject('NOTIFICATION_SERVICE') private client: ClientKafka*/) {
        
        const kafka = new Kafka({
            brokers: ['localhost:9092']
        });
        
        this.consumer = kafka.consumer({
            groupId: 'notifications-consumer-group'
        });
        
        this.subscribeToTopic();

        //TOPIC CREATION Review if is needed
        /*
        var topicsToCreate = [{
            topic: 'notifications',
            partitions: 1,
            replicationFactor: 2
        }];
        this.newClient.setTopic(topicsToCreate, (error, result) => {
           console.log(`Error creating topic error: ${error}`);
           console.log(`Error creating topic error: ${result}`);
        });
        */                
    }

    async subscribeToTopic() {
        await this.consumer.connect();
        await this.consumer.subscribe({ topic: 'notifications', fromBeginning: true })
        await this.consumer.run({
            eachMessage: async ({ topic, partition, message }) => {
                console.log('EachMessage');
                console.log({
                    value: message.value.toString(),
                });
                this.notificationService.sendMessage(message.value.toString());
            }
        })
    
    }
}