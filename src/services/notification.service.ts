import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class NotificationService {
  templatesDir: string;
  constructor(private readonly mailerService: MailerService, private config: ConfigService) {
    this.templatesDir = `${this.config.get<string>('mailer.templatesDir')}/`;
  }

  public sendMessage(strPayload): void {
    console.log('Notification Service: Send Message Called')
    console.log(strPayload);
    let payload = JSON.parse(strPayload);
    console.log(payload);
    
    if(payload.channel === "MAIL")
      this.sendEmail(payload.options, payload.data);
  }
  private sendEmail(options, data) {
    this.mailerService
      .sendMail({
        to: options.to,
        from: options.from,
        subject: options.subject,
        template: `${this.templatesDir}${options.template}`,
        context: data
      })
      .catch((exception) => {console.log(`NotificationService: sendEmail <Exception>: ${exception}`)});
  }
}